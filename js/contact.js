$(document).ready(function(){

    (function($) {
        "use strict";


    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "type the correct answer -_-");

    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                subject: {
                    required: true,
                    minlength: 4
                },
                number: {
                    required: true,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true,
                    minlength: 20

                }
            },
            messages: {
                name: {
                    required: "Vamos, debes tener un nombre, ¿no?",
                    minlength: "tu nombre debe tener más de 2 caracteres, ¿cierto?"
                },
                subject: {
                    required: "Hey, ¿no tienes un asunto?",
                    minlength: "El asunto debe contener mínimo 4 caracteres"
                },
                number: {
                    required: "Oye, debes tener un número, así podemos contactarte",
                    minlength: "Tu número tiene que tener más de 5 caracteres"
                },
                email: {
                    required: "¿Siglo XXI y tú sin E-mail?, creo que debes tener uno"
                },
                message: {
                    required: "Vale, creo que tú debes tener un mensaje o una razón para querer contactarnos, ¿cierto?",
                    minlength: "¿Eso es todo?, ¿en serio?, ¿me estás vacilando?"
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    data: $(form).serialize(),
                    url:"contact_process.php",
                    success: function() {
                        $('#contactForm :input').attr('disabled', 'disabled');
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor','default');
                            $('#success').fadeIn()
                            $('.modal').modal('hide');
		                	$('#success').modal('show');
                        })
                    },
                    error: function() {
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $('#error').fadeIn()
                            $('.modal').modal('hide');
		                	$('#error').modal('show');
                        })
                    }
                })
            }
        })
    })

 })(jQuery)
})
